var Events = require( "../src/index" ),
  should = require( "should" );

var __extends = function ( d, b ) {
  for ( var p in b ) {
    if ( b.hasOwnProperty( p ) ) {
      d[ p ] = b[ p ];
    }
  }
};

describe( "Given an Events api", function () {
  it( "should be defined", function () {
    Events.should.exists;
  } );
  it( "should have an ON function", function () {
    Events.on.should.exists;
  } );
  it( "should have an OFF function", function () {
    Events.off.should.exists;
  } );
  it( "should have an ONCE function", function () {
    Events.once.should.exists;
  } );
  it( "should have an trigger function", function () {
    Events.trigger.should.exists;
  } );
  it( "should have a listenTo function", function () {
    Events.listenTo.should.exists;
  } );
  it( "should have a listenToOnce function", function () {
    Events.listenToOnce.should.exists;
  } );
  it( "should have a stopListening function", function () {
    Events.stopListening.should.exists;
  } );
  describe( "when I attach the Object", function () {

    function Class() {
      this.name = "inerit";
      this.testGeneralChange = "test";
    }
    
    Class.prototype.initialize = function () {
      this.isInitialize = true;
    };

    Class.prototype.initialized = function () {
      this.isInitialized = true;
    };

    __extends( Class.prototype, Events );

    var objectWithEvent = new Class();

    it( "should have a on function", function ( done ) {
      console.log( objectWithEvent );
      objectWithEvent.on( "change:Name", function ( value ) {
        value.should.equal( "hop" );
        done();
      } );

      objectWithEvent.trigger( "change:Name", "hop" );
    } );
    it( "should have a on function", function ( done ) {
      objectWithEvent.on( "change", function ( value ) {
        
        done();
      } );

      objectWithEvent.trigger( "change:testGeneralChange", "hop" );
    } );
  } );
} );