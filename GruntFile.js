var glob = require( "glob" );

module.exports = function ( grunt ) {

  grunt.initConfig( {
    pkg: grunt.file.readJSON( "package.json" ),
    clean: {
      nugget: [ "*.nupkg" ],
      all: [ "*.nupkg", "dist", "docs", "node_modules" ]
    },
    xmlpoke: {
      nuget: {
        options: {
          replacements: [ {
            xpath: "/package/metadata/version",
            value: "<%= pkg.version %>"
          }, {
            xpath: "/package/metadata/id",
            value: "<%= pkg.name %>"
          }, {
            xpath: "/package/metadata/authors",
            value: "<%= pkg.author %>"
          }, {
            xpath: "/package/metadata/description",
            value: "<%= pkg.description %>"
          }, {
            xpath: "/package/metadata/owners",
            value: "<%= pkg.author %>"
          } ]
        },
        files: {
          "Package.nuspec": "nuget.xml"
        }
      }
    },
    shell: {
      test: {
        command: "npm test",
        options: {
          async: false
        }
      },
      npmpublish: {
        command: "npm publish ./",
        options: {
          async: false
        }
      },
      nupack: {
        command: "nuget pack Package.nuspec",
        options: {
          async: false
        }
      },
      nupublish: {
        command: "nuget push <%= pkg.name %>.<%= pkg.version %>.nupkg",
        options: {
          async: false
        }
      }
    },
    uglify: {
      structure: {
        files: {
          "./dist/<%= pkg.name.replace(/js$/i, '') %>.min.js": [ "./dist/<%= pkg.name.replace(/js$/i, '') %>.js" ]
        }
      }
    },
    browserify: {
      dist: {
        src: [ "./src/index.js" ],
        dest: "./dist/<%= pkg.name.replace(/js$/i, '') %>.js",
        options: {
          standalone: "SPEAK<%= pkg.name.replace(/js$/i, '') %>"
        }
      }
    },
    jshint: {
      all: [ "Gruntfile.js", "./src/**/*.js", "./test/**/*.js" ],
      options: {
        curly: true,
        eqeqeq: true,
        immed: false,
        latedef: true,
        quotmark: "double",
        noarg: true,
        forin: true,
        newcap: true,
        sub: true,
        undef: false,
        boss: true,
        strict: false,
        unused: false,
        eqnull: true,
        node: true,
        browser: true,
        expr: "warn"
      }
    },
    watch: {
      src: {
        files: [ "*.js", "*.json", "./test/*.js", "./src/**/*" ],
        tasks: [ "build", "test" ],
        options: {
          livereload: true
        }
      }
    },
    yuidoc: {
      schemajs: {
        name: "<%= pkg.name.replace(/js$/i, '') %>",
        description: "<%= pkg.description %>",
        version: "<%= pkg.version %>",
        url: "<%= pkg.url %>",
        options: {
          paths: "src/",
          outdir: "docs/",
          themedir: "node_modules/yuidoc-bootstrap-theme/",
          helpers: [ "node_modules/yuidoc-bootstrap-theme/helpers/helpers.js" ]
        }
      }
    }
  } );

  grunt.loadNpmTasks( "grunt-browserify" );
  grunt.loadNpmTasks( "grunt-contrib-jshint" );
  grunt.loadNpmTasks( "grunt-contrib-watch" );
  grunt.loadNpmTasks( "grunt-shell-spawn" );
  grunt.loadNpmTasks( "grunt-contrib-uglify" );
  grunt.loadNpmTasks( "grunt-contrib-yuidoc" );
  grunt.loadNpmTasks( "grunt-xmlpoke" );
  grunt.loadNpmTasks( "grunt-contrib-clean" );

  grunt.registerTask( "clientTest", "Running mocha js for all the deps", function () {

    var files = glob.sync( "testClient/**/*.html" );

    var count = 0;
    files.forEach( function ( file ) {

      var property = "shell.cl" + count + ".command";
      grunt.config( property, "mocha-phantomjs " + file );

      count++;
    } );

    grunt.task.run( "shell:test" );

  } );

  grunt.registerTask( "default", [ "build", "test", "minify", "docs", "xml", "clean:nugget" ] );
  grunt.registerTask( "prod", [ "build", "test", "minify", "docs", "xml", "publish", "clean:nugget" ] );
  grunt.registerTask( "build", [ "browserify:dist", "jshint" ] );
  grunt.registerTask( "test", [ "build", "clientTest" ] );
  grunt.registerTask( "minify", [ "uglify:structure" ] );
  grunt.registerTask( "docs", [ "yuidoc:schemajs" ] );
  grunt.registerTask( "publish", [ "shell:npmpublish" ] );
  grunt.registerTask( "xml", [ "xmlpoke:nuget" ] );

  grunt.registerTask( "start", function () {
    grunt.util.spawn( {
      cmd: "node",
      args: [ "docs.js" ]
    } );
    grunt.task.run( "watch" );
  } );
};